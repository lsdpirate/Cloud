from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from jinja2 import Environment, PackageLoader

app = Flask('Cloud')
app.secret_key = 'c\xaa\xb7\xd3\x05?\x9f\x1d\xe7$\x88\x0e\xf3<1\x05\xfc\xc5B\x0co{\xec@'
app.config.from_object('config')
db = SQLAlchemy(app)


import flask.ext.login as flask_login
import core.users.user_management as user_management

login_manager = flask_login.LoginManager()
login_manager.init_app(app)

env = Environment(loader= PackageLoader('core.webapp', 'templates'))
import core.webapp.views
login_manager.login_view = 'login'



def run_webinterface():
    user_management.load_users()
    import time
    time.sleep(1)
    app.debug = True
    app.run()
    user_management.write_users()


