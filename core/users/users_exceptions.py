class UserNotFoundException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'The user \'{}\' was not found'.format(self.value)

class PasswordMismatchException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Invalid password was entered for user {}'.format(self.value)
