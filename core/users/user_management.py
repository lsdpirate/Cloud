import core.filesystem.filesystem_manager as filesystem_manager
from core.webinterface import db
from core.users.users_exceptions import UserNotFoundException, PasswordMismatchException

class User(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique = True)
    password = db.Column(db.String(64))

    def get_name(self):
        return self.username

    def get_serialization(self):
        return self.username + ";" + self.password

    def reload_folder_structure(self):
        self.directory = filesystem_manager.get_user_file_tree(self.username)

    def passwords_match(self, to_test):
        return to_test == self.password

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.username)

    def __str__(self):
        return self.username

    def __repr__(self):
        return self.__str__()


def add_user(username, password):
    try:
        search_user(username)
    except UserNotFoundException:
            """
            An user with this username was not found, we can
            proceed to add a new one with said name.
            """
            to_add = User(username = username, password = password)

            userpath = filesystem_manager.get_user_path(username)
            if not filesystem_manager.path_exists(userpath):
                filesystem_manager.make_user_directory(username)

            db.session.add(to_add)
            db.session.commit()
            return to_add
    raise Exception('Another user with this name already exists!')

def remove_user(username):
    """
    DEPRECATED -- need to reimplement with db
    """
    to_remove = search_user(username)
    if not (to_remove == 'NOT_FOUND'):
        USERS.remove(to_remove)
    return to_remove

def search_user(username):
    """
    Queries the database to find an user with the entered username
    and returns it.
    Raises an exception if no user was found.
    """
    result = User.query.filter(User.username == str(username)).first()

    if(result == None):
        raise UserNotFoundException(username)
    else:
        return result


def check_login(username, password):
    """
    Checks if a username-password pair is valid
    to make a login
    """
    user = search_user(username)
    print('Found user {}'.format(user))
    if user.passwords_match(password):
        print('Password match for {}'.format(user))
        return user
    else:
        raise PasswordMismatchException(user.get_name())
