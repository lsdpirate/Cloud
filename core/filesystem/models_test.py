import fs_models
from fs_models import File, Directory

file1 = fs_models.File("first", "13", "today")
file2 = File("second", "2", "today")
folder1 = Directory("firstFolder")
subfile1 = File("subFile", "--", "today")
root = Directory("root")

root.add_file(file1)
root.add_file(file2)
root.add_file(folder1)
folder1.add_file(subfile1)

print("TESTING JSON...")
print(file1.to_json())
print(folder1.to_json())
print(root.to_json())
