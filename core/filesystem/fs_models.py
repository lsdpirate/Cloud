import json

class File():
    
    def __init__(self, name, size, date):
        self.name = name
        self.size = size
        self.date = date

    def set_name(self, new_name):
        self.name = new_name

    def get_name(self):
        return self.name

    def get_size(self):
        return self.size

    def get_last_modified(self):
        return self.date

    def __str__(self):
        return self.name + " " + str(self.size)
    
    def to_json(self):
        return json.dumps(self.to_dict_object())

    def to_dict_object(self):
        result = {
            'file' : {
                'name' : self.name,
                'size' : str(self.size),
                'date' : self.date,
            }
        }

        return result
        

class Directory(File):

    def __init__(self, name):
        self.name = name
        self.files = []

    def get_name(self):
        return self.name

    def get_files(self):
        return self.files

    def get_file(self, file):
        return file in self.files

    def add_file(self, to_add):
        self.files.append(to_add)

    def remove_file(self, to_remove):
        self.files.remove(to_remove)

    def __str__(self):
        result = self.name + "\n\t\t"
        for file in self.files:
            result += file.__str__()
            result += "\n"
            result += "\t\t"
        return result

    def to_json(self):
        """
        Returns the hierarchy of this folder
        as a JSON/dictionary.
        """
        return json.dumps(self.to_dict_object())

    def to_dict_object(self):
        result = []
        for file in self.files:
            result.append(file.to_dict_object())

        result = {
         'folder' :{
            'name' : self.name,
            'children' : result,
         },
        }

        return result


