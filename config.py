import os

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

ROOT_FS_PATH = './corefs'

secret_key = '\xc7wa\xect\xd6\xcd\xb4\xd9\xe6x^\xae6^Qt\xa2\xff\x15\xf1\xba\xe4\xe4'